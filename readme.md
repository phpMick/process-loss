# Process Loss Application

## Fault Reporting System for Manufacturing Company

Using Laravel and Vue.js.

## Maintenance  Screens

![3.png](https://bitbucket.org/repo/ekrooba/images/1146486863-3.png)

## Entry Screens - Designed for tablet use only

![1.png](https://bitbucket.org/repo/ekrooba/images/1677106660-1.png)

![2.png](https://bitbucket.org/repo/ekrooba/images/3115589182-2.png)