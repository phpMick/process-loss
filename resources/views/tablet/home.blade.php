<!DOCTYPE html>
<html lang="en" class="black">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <meta name="mobile-web-app-capable" content="yes">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Process Loss</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/tablet.css') }}">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body class="black">

{{--<header>Process Loss</header>--}}



<div id="app">




    <ch-process-fault processes="{{$processTypes}}"> </ch-process-fault>


</div>



<!-- Scripts -->
<script src="{{ elixir('js/all.js') }}"></script>
<script src="{{ elixir('js/tablet.js') }}"></script>
<script src="{{ elixir('js/app.js') }}"></script>







<script>




    //this would be better in the Vue component - just pasted from old application
    //todo come back to this - not sure how

    /**
     * MB Modified  from
     * http://twitter.github.io/typeahead.js/examples/
     * This is the lookup for the part_no
     */
//this is the Bloodhound, the suggestion engine
    var productList = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('part_no'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        //Maybe I need a prefetch
        // prefetch: '../data/films/post_1960.json',
        remote: {
            url: 'getProducts/%QUERY',
            wildcard: '%QUERY'
        }
    });



    $('#scrollable-dropdown-menu .typeahead').typeahead(null, {
        name: 'PARTS',
        limit: 30,
        highlight: true,
        display: 'part_no',
        source: productList, //an instance of bloodhound
        templates: {
            empty: [
                '<div class="empty-message">',
                'Not Found',
                '</div>'
            ].join('\n'),

            suggestion: function(data) {
                return '<div><strong>' + data.part_no +  '</strong> – ' + data.product_description + '</div>'
            }
        }
    });


    $('.typeahead').on('keydown', function(event) {
        // Define tab key
        var e = jQuery.Event("keydown");
        e.keyCode = e.which = 9; // 9 == tab

        if (event.which == 13) // if pressing enter
            $('.typeahead').trigger(e); // trigger "tab" key - which works as "enter"
    });


    //display the description for this part_no
    $('.typeahead').on('typeahead:selected', function (evt, item) {
        $('#description').text(item.product_description);
        $('#hidden_part_no').val(item.part_no);
    });



</script>

    </body>
</html>



