@extends('layouts.appTopNav')

@section('head')

@endsection

@section('content')

    <input type="hidden" name="_token" value="{ csrf_token() }" id="token">


<div class="container">

    <div class="panel panel-primary">
        <div class="panel-heading" id="panel-head">
            {{$niceName}}
        </div>

    @include('generic.partials.table')

    </div>



</div>


@endsection


@section('js')

@include('generic.partials.script');

@endsection






