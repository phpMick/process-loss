<script>


    $(document).ready(function() {
        $("[id^=date_selected_search]").val(moment().format('YYYY-MM-DD'));

        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: {table: "{{$table}}"}
        });

        //setup the editor
        editor = new $.fn.dataTable.Editor({


                 "ajax": {
                "url": "/{{$table}}Ajax",

                "type": "POST",
                data: {table: "{{$table}}"}
            },




    table: "#generic_table",

    idSrc: '{{$table}}.id',

    fields: [{!!  $editorFields !!} ]

        });




        //setup the Datatable
        table = $('#generic_table').DataTable({ //took the var off to intentionally make it global

            "ajax": {
                "url": "/{{$table}}Ajax",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                "type": "POST",

                "data": function(d){
                    d.table = "{{$table}}";
                    @if($table=="losses")
                        d.date_selected = GetSearchText();
                    @endif
                },

            },
            "columns": [
                {
                    data: null,

                    defaultContent: '',
                    className: 'select-checkbox',
                    orderable: false
                },

                {!!   $tableFields !!}


            ],
            order: [1, 'asc'],
            dom: "Bfrtip",
            responsive: false,//true wont let me hide columns
            "scrollY": "600px",
            "scrollCollapse": true,
            {!! $columnDefs  !!}
            "paging": false,
            select: {
                style: 'single',
                selector: 'td:first-child',
                blurable: true

            },

            buttons: [{!! $buttons !!}],

        });

        editor
        /*.on('open', function (e, mode, action) {
         if (mode === 'main') {
         table.keys.disable();
         }
         })
         */
                .on('close', function () {
                    //table.keys.enable();

                    table.rows().deselect();
                })

    });

</script>

