<div class="panel-body">

    <table class="table table-responsive" id="generic_table">
        <thead>
        <tr>


            @if($edit)
                <th></th>
            @endif

            @foreach($headers as $header)

                <th>{{$header}}</th>

            @endforeach



        </tr>
        </thead>

    </table>

</div>