<!DOCTYPE html>
<html lang="en" class="full">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
    <title>Process Loss Application</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Churchill Process Loss') }}</title>

    <!-- Styles -->

    <link rel="stylesheet" href="{{ elixir('/css/all.css') }}">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body  class="full">




    @yield('content')


<!-- Scripts -->

    <script src="{{ elixir('js/all.js') }}"></script>
    <script src="{{ elixir('js/app.js') }}"></script>

</body>

<style>
.full{
    position:fixed;
    top:0px;
    bottom:0px;
    left:0px;
    right:0px;
}

</style>

</html>
