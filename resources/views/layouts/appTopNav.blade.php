<!DOCTYPE html>
<html lang="en">

<head>
    {{-- Based on http://www.prepbootstrap.com/bootstrap-theme/dark-admin--}}
    {{--https://github.com/BlackrockDigital/startbootstrap-simple-sidebar--}}


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Churchill - Process Losses</title>

    <link rel="stylesheet" href="{{ elixir('css/all.css') }}">



    <!-- Bootstrap docs say add this to stop navbar overlapping-->
    <style>
        body { padding-top: 70px; }
    </style>


    @yield('head')

</head>

<body>


<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home">
                <img  style="height: 100%;" alt="Churchill" src="/images/churchill_logo.png" class="img-responsive">
            </a>
        </div>


        @include('partials.navbar')


    </div>
</nav>




@yield('content')

<script src="{{ elixir('js/all.js') }}"></script>
<script src="{{ elixir('js/app.js') }}"></script>



<script>

    // todo move this

    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
    });

</script>

</body>
@yield('js')
</html>
