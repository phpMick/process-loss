<div class="collapse navbar-collapse" id="navbar">
    <ul class="nav navbar-nav">



        <li class="{{ set_active('Process Types') }}"><a href="/processTypes">Process Types<span class="sr-only"></span></a></li>

        <li class="{{ set_active('Streams') }}"><a href="/streams">Streams<span class="sr-only"></span></a></li>

        <li class="{{ set_active('Users') }}"><a href="/users">Users<span class="sr-only"></span></a></li>

        <li class="{{ set_active('Faults') }}"><a href="/faults">Faults<span class="sr-only"></span></a></li>

    </ul>

        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown pull-right">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ auth()->user()->username }} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">

                <li><a href="{{ url('/logout') }}">Logout</a></li>

                <li><a href="/users/{{ auth()->user()->id }}/edit">Change Password</a></li>
            </ul>
        </li>
    </ul>
</div>













