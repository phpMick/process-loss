@extends('layouts.appTopNav')

@section('head')
@endsection

@section('content')

    <input type="hidden" name="_token" value="{ csrf_token() }" id="token">


    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading" id="panel-head">
                Assign Faults to Process Areas
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    {!! Form::label('selector_id', 'Stream:', ['class' => 'col-xs-6 col-sm-2 form-control-label']) !!}
                    <div class="col-xs-6 col-sm-2">
                        {!! Form::select('stream_id', $streams,null, array('class' => 'form-control','id'=>'stream_id')) !!}
                    </div>
                </div>
                <div class="form-group row">
                    {!! Form::label('processType_id', 'Process Type:', ['class' => 'col-xs-6 col-sm-2 form-control-label']) !!}
                    <div class="col-xs-6 col-sm-2">
                        {!! Form::select('processType_id', $processTypes,null, array('class' => 'form-control','id'=>'process_type_id')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container">

        <div class="panel panel-primary">
            <div class="panel-heading" id="panel-head">
                Available Faults
            </div>

            <div class="panel-body">

                <table class="table table-responsive" id="faultProcess_table">
                    <thead>
                    <tr>


                        <th></th>


                        <th>Rank</th>


                        <th>Fault</th>


                    </tr>
                    </thead>

                </table>

            </div>


            @endsection


            @section('js')




                <script>


                    $(document).ready(function () {


                        $('#stream_id,#process_type_id').on( 'change', function () {
                            table.ajax.reload();

                        });

                        function getStream() {
                            return $('#stream_id').val();
                        }

                        function getProcess() {
                            return $('#process_type_id').val()
                        }


                        $.ajaxSetup({
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'

                        });

                        //setup the editor
                        editor = new $.fn.dataTable.Editor({

                            ajax: {
                                    url: '/faultProcessAjax',
                                "type": "POST",
                                "data": function(d){
                                    d.stream_id = getStream();
                                    d.process_type_id = getProcess();
                                }
                            },

                            table: "#faultProcess_table",
                            idSrc: 'fault_type_process_type.id',
                            fields: [{
                                label: 'Rank:',
                                name: 'fault_type_process_type.rank',
                                type:  "select",
                                options: [
                                    { label: "1", value: "1" },
                                    { label: "2", value: "2" },
                                    { label: "3", value: "3" },
                                    { label: "4",  value: "4" },
                                    { label: "5",  value: "5" },
                                    { label: "6",  value: "6" },
                                    { label: "7",  value: "7" },
                                    { label: "8",  value: "8" },
                                    { label: "9",  value: "9" }
                                ]
                            }, {


                                label: 'Description:',
                                name: 'fault_type_process_type.fault_id',

                                type: 'select2',
                                opts: {
                                    multiple:false,

                                    minimumInputLength: 3,
                                    ajax: {
                                        url: "/autocomplete/faults/",
                                        dataType: "JSON",
                                        delay: 250,
                                        data: function (params) {
                                            return {
                                                query: params.term, // search term
                                                page: params.page
                                            };
                                        },
                                        //https://select2.github.io/examples.html
                                        processResults: function (data, params) {
                                            // parse the results into the format expected by Select2
                                            // since we are using custom formatting functions we do not need to
                                            // alter the remote JSON data, except to indicate that infinite
                                            // scrolling can be used
                                            params.page = params.page || 1;
                                            return {
                                                results: data.items,
                                                pagination: {
                                                    more: (params.page * 30) < data.total_count
                                                }
                                            };
                                        },
                                        cache: true
                                    },

                                    minimumInputLength: 3,
                                    templateResult: function(data) {
                                        return data.text;
                                    },
                                    templateSelection: function(data) {
                                        return data.text;
                                    },

                                    }
                                }
                            ]
                        });


                        //setup the Datatable
                        table = $('#faultProcess_table').DataTable({ //took the var off to intentionally make it global

                            "ajax": {
                                "url": "/faultProcessAjax",
                                headers: {
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                "type": "POST",
                                "data": function(d){
                                    d.stream_id = getStream();
                                    d.process_type_id = getProcess();
                                }
                            },
                            "columns": [
                                {
                                    data: null,
                                    defaultContent: '',
                                    className: 'select-checkbox',
                                    orderable: false
                                },

                                {'data': 'fault_type_process_type.rank'},
                                //config in blade

                                {'data': '{{$selectionDB}}.faults.description'},

                            ],
                            order: [1, 'asc'],
                            dom: "Bfrtip",
                            responsive: false,//true wont let me hide columns
                            "scrollY": "600px",
                            "scrollCollapse": true,
                            "paging": false,
                            select: {
                                style: 'single',
                                selector: 'td:first-child',
                                blurable: true
                            },
                            buttons: [{extend:'remove', editor: editor},{extend:'create', editor: editor},{extend:'edit', editor: editor},],

                        });

                        editor
                            .on('close', function () {
                                //table.keys.enable();
                                table.rows().deselect();
                            })
                    });


                </script>




@endsection






