<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();

//need to add a get route now
Route::get('/logout', 'Auth\LoginController@logout');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');



//-----------Datatables

Route::get('processTypes', 'ProcessTypesController@index');
Route::get('/process_typesAjax', 'ProcessTypesController@datatablesAjax');
Route::post('/process_typesAjax', 'ProcessTypesController@datatablesAjax');

Route::get('streams', 'StreamsController@index');
Route::get('/streamsAjax', 'StreamsController@datatablesAjax');
Route::post('/streamsAjax', 'StreamsController@datatablesAjax');



Route::get('/faultProcessAjax', 'FaultProcessController@datatablesAjax');
Route::post('/faultProcessAjax', 'FaultProcessController@datatablesAjax');


Route::get('/view_process_loss_usersAjax', 'UsersController@datatablesAjax');
Route::post('/view_process_loss_usersAjax', 'UsersController@datatablesAjax');


//---------------Resources
Route::get('users', 'UsersController@index');
Route::get('faults', 'FaultProcessController@index');

//---------------Autocomplete

Route::get('/autocomplete/faults', 'FaultProcessController@autocompleteFaults');

//This is the usual product number Twitter Typeahead

Route::get('getProducts/{searchString}', 'HomeController@getProducts');
Route::get('validateProduct', 'HomeController@validateProduct');



Route::get('/getFaults', 'HomeController@getFaults');

//adding a fault
Route::post('/addFault', 'HomeController@addFault');


//Catch all
Route::any('{query}',
    function () {
        return redirect('/');
    })
    ->where('query', '.*');
