<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TabletTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testLogin()
    {

        $this->visit('/login')
            ->type('test', 'username')
            ->type('test', 'password')
            ->press('Login')
            ->seePageIs('/home');


    }
}
