{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('fault_type_id', 'Fault_type_id:') !!}
			{!! Form::text('fault_type_id') !!}
		</li>
		<li>
			{!! Form::label('process_type_id', 'Process_type_id:') !!}
			{!! Form::text('process_type_id') !!}
		</li>
		<li>
			{!! Form::label('stream_id', 'Stream_id:') !!}
			{!! Form::text('stream_id') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}