{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('product_id', 'Product_id:') !!}
			{!! Form::text('product_id') !!}
		</li>
		<li>
			{!! Form::label('user_id', 'User_id:') !!}
			{!! Form::text('user_id') !!}
		</li>
		<li>
			{!! Form::label('fault_type_id', 'Fault_type_id:') !!}
			{!! Form::text('fault_type_id') !!}
		</li>
		<li>
			{!! Form::label('primary_fault', 'Primary_fault:') !!}
			{!! Form::text('primary_fault') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}