<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStreamsTable extends Migration {

	public function up()
	{
		Schema::create('streams', function(Blueprint $table) {
			$table->increments('id');
			$table->string('description', 255);
		});
	}

	public function down()
	{
		Schema::drop('streams');
	}
}