<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaultsTypesTable extends Migration {

	public function up()
	{
		Schema::create('faults_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('description', 255);
		});
	}

	public function down()
	{
		Schema::drop('faults_types');
	}
}