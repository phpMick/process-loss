<?php 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});


Route::resource('faulttype', 'FaultTypeController');
Route::resource('processtype', 'ProcessTypeController');
Route::resource('fault_type_process_type', 'Fault_type_process_typeController');
Route::resource('recordedfault', 'RecordedFaultController');
Route::resource('inputtype', 'InputTypeController');
Route::resource('stream', 'StreamController');
