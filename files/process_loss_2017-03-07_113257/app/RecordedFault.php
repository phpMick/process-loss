<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordedFault extends Model {

	protected $table = 'recorded_faults';
	public $timestamps = true;

}