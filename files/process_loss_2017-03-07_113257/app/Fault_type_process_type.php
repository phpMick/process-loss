<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fault_type_process_type extends Model {

	protected $table = 'fault_type_process_type';
	public $timestamps = false;

}