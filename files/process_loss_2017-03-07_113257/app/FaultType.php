<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaultType extends Model {

	protected $table = 'faults_types';
	public $timestamps = false;

}