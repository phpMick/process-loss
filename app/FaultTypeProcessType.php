<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaultTypeProcessType extends Model {

	protected $table = 'fault_type_process_type';
	public $timestamps = false;

}