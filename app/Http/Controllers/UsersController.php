<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use
    DataTables\Editor,
    DataTables\Editor\Field;


use DataTables\Editor\MJoin;

include( app_path() . "/Libraries/DataTables/DataTables.php" );


class UsersController extends DatatablesController
{
    protected $niceName = 'Maintain Users'; //The title for the datatable
    protected $table = "view_process_loss_users";

    protected $specialButtons = "{extend:'edit', editor: editor}"; //we only get the edit because the users are set up in the Web User Manager Application

    protected $headers = array('Username','Process Types'); //The datatable headers
    protected $tableFields = "
      { 'data': 'view_process_loss_users.username'},
      { 'data': 'process_types','render' : '[,].description'}
    ";



    protected $editorFields = "{
					label: 'Username',
					name: 'view_process_loss_users.username'
					},
					{
					label: 'Process Type',
					name:   'process_types[].id',
					type:  'select',
					multiple:   true,
					placeholder:  'Select Process Types'
					}";








    /**
     * This is the Datatables Ajax for the Selectors table
     *
     *
     */
    public function datatablesAjax()
    {

        global $db;//this is needed to get Datatables to work with Laravel

        $postData = $_POST;

        Editor::inst($db, 'view_process_loss_users')
            ->fields(
                Field::inst('view_process_loss_users.id'),
                Field::inst('view_process_loss_users.username')
            )

            ->join(
                Mjoin::inst( 'process_types' )//table to link to
                ->link('view_process_loss_users.id','process_type_user.user_id' ) //left table id, name in link table
                ->link('process_types.id','process_type_user.process_type_id' ) //right table id, name in link table

                ->order('process_types.description asc')// order of joined data - optional
                ->fields(
                    Field::inst( 'id' ) //first field read from joined table
                    ->options( 'process_types', 'id', 'description' )
                        ->validator( 'Validate::notEmpty' ),
                    Field::inst( 'description') //second field read from joined table
                )
            )
            ->process($postData)
            ->json();

    }
}