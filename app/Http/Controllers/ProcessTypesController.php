<?php
/*
 * Uses a Datatable, to maintain the Process Types (factory locations)
 *
 *
 */

namespace App\Http\Controllers;



use App\Http\Requests;

use
    DataTables\Editor,
    DataTables\Editor\Field;

include( app_path() . "/Libraries/DataTables/DataTables.php" );


class ProcessTypesController extends DatatablesController
{
    protected $niceName = 'Maintain Process Types'; //The title for the datatable
    protected $table = "process_types";

    protected $headers = array('Description', 'Input Type'); //The datatable headers

    //The fields we are editing
    protected $editorFields = "{
        label: 'Process:',
        name: 'process_types.description'
        },
        {
        label: 'Input Type:',
        name: 'process_types.input_type_id',
        type:'select',
        placeholder: 'Select an Input Type'
        }
        ";

    //The actual database fields
    protected $tableFields = "
      { 'data': 'process_types.description'},
      { 'data': 'input_types.description',editField:'process_types.input_type_id'}
    ";

    protected $specialButtons = "";



    /**
     * This is the Datatables Ajax for the Selectors table
     *
     *
     */
    public function datatablesAjax()
    {

        global $db;//this is needed to get Datatables to work with Laravel

        $postData = $_POST;


        Editor::inst($db, 'process_types')
            ->fields(
                Field::inst("process_types.id"),
                Field::inst('process_types.description')->validator('Validate::unique'),
                //Dont forget the join
                //this is how to do a select //this table.foreign key
                Field::inst( 'process_types.input_type_id' )
                    //joined table fields
                    ->options('input_types','id','description')
                    ->validator( 'Validate::dbValues' ),
                //joined table.field to display
                Field::inst('input_types.description')
                //joined table, id, foreign key in this table
            )->leftjoin('input_types', 'input_types.id', '=', 'process_types.input_type_id')
            ->process($postData)
            ->json();

    }
}