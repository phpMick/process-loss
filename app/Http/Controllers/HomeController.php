<?php

/*
 * This is for the initial home page
 *
 *
 *
 */

namespace App\Http\Controllers;

//Models
use App\WidgetType;
use App\User;
use App\Product;
use App\RecordedFault;
use App\ProcessType;

//Laravel
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class HomeController extends AuthOnlyController
{
    /**
     * Show the tablet home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);

        //Only get process types which have some faults assigned
        $processTypes = $user->processTypesWithFaults();

        return view('tablet.home', compact('processTypes'));
    }


    /**
     * This is the Ajax for the Typeahead.
     * It now goes to Elasticsearch via Elastiquent
     * http://artisantinkerer.github.io/2016/02/12/pizza-hut-pickle.html
     * @author MB
     *
     * @param $searchString
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts($searchString)
    {
        //This is what I am doing below
        /*       curl -XGET "http://devwebapplications-02.churchill1795.local:9200/parts/parts/_search" -d '
       {
           "query": {
               "match": {
                   "PART_NO": "bel"
               }
           }
       }*/
        $products = Product::complexSearch(array(
            'body' => array(
                'query' => array(
                    'match' => array(
                        'part_no' => $searchString
                    )
                )
            )
        ));

        //products is an Elastiquentcollection and needs to be an array

        $arrProducts = $products->toArray();
        return response()->json( $arrProducts);

    }



    /**
     * Ajax POST when the user leaves the Typeahead
     * Validates the product code
     * $retArray['status'] can be :
     *  available - product is in the available products table
     *  valid - not in available but still a valid part
     *  if not valid 404 is thrown
     *
     * @param $searchString
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateProduct(Request $request)
    {
        return Product::validatePartNumber($request->part_no);

    }



    /**
     * This is an AJAX call, to record a new fault.
     *
     * @param Request $request
     */
    public function addFault(Request $request){

        RecordedFault::create(array(
            'product_id' => $request->product_id,
            'user_id' => Auth::id(),
            'fault_type_id' => $request->fault_id,
            'primary_fault' => $request->primary_fault
        ));

    }



    /**
     * AJAX: Returns faults for this
     *  product stream.
     *
     * @param Request $request
     */
    public function getFaults(Request $request){

        return  ProcessType::getFaults($request->product_id,$request->process_type_id);

    }




}
