<?php 

namespace App\Http\Controllers;

use App\ProcessType;
use App\Stream;
use App\Fault;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Gate;
use
    DataTables\Editor,
    DataTables\Editor\Field;


use DataTables\Editor\Options;



include( app_path() . "/Libraries/DataTables/DataTables.php" );

class FaultProcessController extends AuthOnlyController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()

  {
      //standard Laravel auth
      if (Gate::denies('fault_type_process_type')) {
          return redirect()->route('home');
      }

      //get the data we need to display
      $streams = Stream::pluck('description', 'id');
      $processTypes = ProcessType::pluck('description', 'id');
      $faults = Fault::pluck('description', 'id');

      //db has a different name on the live server which is needed for joins
      $selectionDB = env('DB_DATABASE_SELECTIONDB');

      return view('faults.list', compact('streams','processTypes','faults','selectionDB'));

  }


    /**
     * This is the Datatables Ajax for the Losses table.
     * If this looks confusing skim through this https://editor.datatables.net/
     *
     */
    public function datatablesAjax()
    {
        //table is fault_type_process_type

        global $db;//this is needed to get Datatables to work with Laravel

        $postData = $_POST;

        $process_type_id = $postData['process_type_id'];
        $stream_id = $postData['stream_id'];

        //the Selection Database will have a different name in live;
        $selectionDB = env('DB_DATABASE_SELECTIONDB');


        //if we are editing, we don't need these
        if(isset($postData['action'])) {
            if ($postData['action'] != 'edit') {
                //hack these into the array, so that values from the selects are used
                $postData['data'][0]['fault_type_process_type']['stream_id'] = $stream_id;
                $postData['data'][0]['fault_type_process_type']['process_type_id'] = $process_type_id;
            }
        }

        Editor::inst($db, 'fault_type_process_type')
            ->where("stream_id",$stream_id)
            ->where("process_type_id",$process_type_id)
            ->fields(


                Field::inst("fault_type_process_type.stream_id"),
                Field::inst("fault_type_process_type.process_type_id"),

                Field::inst("fault_type_process_type.id"),
                Field::inst("fault_type_process_type.rank"),

                Field::inst( 'fault_type_process_type.fault_id' )
                    ->options( Options::inst()
                        ->table( $selectionDB.'.faults' )
                        ->value( 'id' )
                        ->label( 'description' )
                    )
                    ->validator( 'Validate::dbValues' ),
                Field::inst( $selectionDB . '.faults.description' )


            )
            ->leftjoin($selectionDB. '.faults', 'fault_type_process_type.fault_id', '=', $selectionDB.".faults.id")
            ->process($postData)
            ->json();
    }


    /**
     * This is used by the Datatables Editor to
     * do the fault description auto complete
     *
     * http://laravel-tricks.com/tricks/jquery-ui-autocomplete-and-subsequent-dropdown-based-on-autocomplete-value
     * @return string
     */

    public function autocompleteFaults(Request $request)
    {

        $selectionDB = env('DB_DATABASE_SELECTIONDB');

        $term      = Input::get('query');


        $search    = DB::select(
            "
            SELECT id ,description as text
            FROM " . $selectionDB . ".faults
            WHERE MATCH (description )
            AGAINST ('+{$term}*' IN BOOLEAN MODE)
            "
        );
        foreach ($search as $result) {
            $faults['items'][] = $result;
        }
        return json_encode($faults);
    }

  
}

?>