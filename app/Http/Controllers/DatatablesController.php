<?php

/*
 * This just encapsulates some of the configuration for the Datatables https://datatables.net/
 * Extend it if you need a controller which needs a Datatable.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\AccessManager;
use Illuminate\Support\Facades\Redirect;

use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Join,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;

use App\Product;
use App\Selector;
use App\Area;
use Illuminate\Support\Facades\Gate;

include( app_path() . "/Libraries/DataTables/DataTables.php" );
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

abstract class DatatablesController extends AuthOnlyController

{

    protected $view = 'generic.list';
    protected $columnDefs = '';



    /**
     *
     * Check permissions and show the view
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //standard Laravel authorisation
        if (Gate::denies($this->table)) {
            return redirect()->route('home');
        }

        //fields required by the Datatable
        $headers = $this->headers;
        $editorFields = $this->editorFields;
        $tableFields = $this->tableFields;
        $columnDefs = $this->columnDefs;


        $niceName = $this->niceName; //name to display on the panel
        $table = $this->table; //database table we editing

        //$fieldsCount = sizeof($fields);
        $edit = true;
        $buttons = "";

        //get the permission for this user for this table
        $permissionSum = AccessManager::getPermissionSum($request, $this->table);

        //get Datatables buttons for this level of permission
        if ($permissionSum == 0) {
            return Redirect::route('home')->withErrors("You do not have permission to access this part of the system.");
        } else {

            if($this->specialButtons === "") {//defined in the module controllers eg Lossescontroller

                AccessManager::getAllowedButtons($this->table, $buttons, $request, $edit);
            }else{
                $buttons =$this->specialButtons;
            }
        }


        return view($this->view, compact('table', 'headers', 'editorFields','tableFields', 'edit', 'buttons', 'niceName','columnDefs'));

    }







}









