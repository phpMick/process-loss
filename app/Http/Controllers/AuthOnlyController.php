<?php
/**
 * This adds the Laravel authorisation to any Controller.
 * Just extend it and the user will need to be logged in access
 * any method in your controller.
 *
 */

namespace App\Http\Controllers;


use App\Http\Requests;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

abstract class AuthOnlyController extends Controller
{


    /**
     * This just does the Auth for controllers which need it
     * Users need to be logged in to get past this.
     * Extend it.
     *
     * " In Laravel 5.3, you can't access the session or authenticated user in your controller's constructor because the middleware has not run yet."
     * https://laravel.com/docs/5.3/upgrade
     *
     * AuthOnlyController constructor.
     */


    public function __construct()
    {
        $this->middleware('auth');


        $this->middleware(function ($request, $next) {
            $user = Auth::user();
            View::share( 'loggedInUser' ,  $user );

            return $next($request);
        });

        $this->middleware('permissions');

    }




}
