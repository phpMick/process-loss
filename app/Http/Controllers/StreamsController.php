<?php

/*
 * Uses a Datatable, to maintain the Streams (eg Cups, Flat , Hollow)
 *
 *
 */


namespace App\Http\Controllers;

use App\Http\Requests;

use
    DataTables\Editor,
    DataTables\Editor\Field;

include( app_path() . "/Libraries/DataTables/DataTables.php" );


class StreamsController extends DatatablesController
{
    protected $niceName = 'Maintain Streams'; //The title for the datatable
    protected $table = "streams";


    protected $headers = array('Description'); //The datatable headers


    protected $editorFields = "{
					label: 'Description',
					name: 'streams.description'
					}
					
					";

    protected $tableFields = "
      { 'data': 'streams.description'}
    ";



    protected $specialButtons = "";



    /**
     * This is the Datatables Ajax for the Selectors table
     *
     *
     */
    public function datatablesAjax()
    {

        global $db;//this is needed to get Datatables to work with Laravel

        $postData = $_POST;


        Editor::inst($db, 'streams')
            ->fields(
                Field::inst('streams.id'),
                Field::inst('streams.description')->validator('Validate::unique')
            )
            ->process($postData)
            ->json();

    }
}