<?php

use App\Product;

/**
 * Set active page
 * https://laracasts.com/discuss/channels/general-discussion/navbar-and-active-elements
 * @param string $uri
 * @return string
 */

//https://laracasts.com/discuss/channels/general-discussion/navbar-and-active-elements
function set_active($uri)
{
    return Request::is($uri) ? 'active' : '';
}



/**
 * Give me a part_no and I will return the product_id
 * @param $part_no
 * @return mixed
 */
function getProductID($part_no)
{
    if ($part_no != null) {
        $product = Product::where('part_no', '=', $part_no)->firstOrFail();
        return $product->id;
    } else {
        return null;

    }
}


/**
 * Give me a product_id and I will return the part_no
 * @param $part_no
 * @return mixed
 */
function getPartNo($product_id){
    if ($product_id!=null) {
        $product = Product::where('id', '=', $product_id)->firstOrFail();
        return $product->part_no;
    }else{
        return null;
    }

}




?>


