<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
class ProcessType extends Model {

	protected $table = 'process_types';
	public $timestamps = false;


    /**
     * Returns users which have the process type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {

        return $this->belongsToMany('App\Users');

    }


    /**
     * Returns faults for this Process Type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function faults()
    {

        return $this->belongsToMany('App\Faults');

    }



    /**
     * AJAX: Returns faults for this
     *  product stream.
     *
     * @param Request $request
     */
    public static function getFaults($product_id,$process_type_id){

        /*select ftpt.fault_id, f.description from
        productsDB.products p
        inner join streams s ON s.description = p.part_product_family
        inner join fault_type_process_type ftpt ON ftpt.stream_id = s.id
        inner join dev_selection.faults f ON ftpt.fault_id = f.id

        where p.id = 108213
        and ftpt.process_type_id = 1;*/

        $selectionDB = env('DB_DATABASE_SELECTIONDB');

        $faults = DB::table('productsDB.products')
            ->join('streams','streams.description','=','productsDB.products.part_product_family')
            ->join('fault_type_process_type','fault_type_process_type.stream_id','=','streams.id')
            ->join($selectionDB.'.faults',$selectionDB.'.faults.id','=','fault_type_process_type.fault_id')
            ->select($selectionDB.'.faults.id',$selectionDB.'.faults.description')
            ->where('productsDB.products.id','=',$product_id)
            ->where('fault_type_process_type.process_type_id','=',$process_type_id)
            ->get();

        if(count($faults)) {
            return response()->json(self::addCountersField($faults));

        }else{
            return response()->json(['error' => 'No faults assigned to this process/stream.'], 404); // Status code here

        }

    }

    /**
     * Just adds a column to the faults
     *
     * @param $faults
     * @return mixed
     */
    private static function addCountersField($faults){

        foreach($faults as $fault){
            $fault->counter = 0;
        }
        return $faults;
    }






}