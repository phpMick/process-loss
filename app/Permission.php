<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;

    protected $connection = 'mysqlUserDB';

    protected $fillable = ['name','object','allow_create','allow_read','allow_update','allow_delete','allow_execute'];

    /**
     * many-to-many relationship method
     *
     * @return QueryBuilder
     */
    public function roles(){
        return $this->belongsToMany('App\Role');
    }





}
