<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordedFault extends Model {

	protected $table = 'recorded_faults';
	public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'fault_type_id', 'product_id','primary_fault'
    ];





}