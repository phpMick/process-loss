<?php namespace App\Traits;


use App\User;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App;
use Log;





trait AccessManager
{
    /**
     *
     * This gets the users permissions for this object.
     *
     * @param $object
     * @param $buttons
     * @param $request
     * @param $edit
     * @return bool
     */
    public static function getAllowedButtons($object,&$buttons,$request,&$edit)
    {

        $permissionsSum = self::getPermissionSum($request, $object);

        //Must have these to be here?
        //if not got any access, redirect with errors
       /* if ($permissionsSum === 0) {
            return Redirect::route('home')->withErrors("You do not have permission to access this part of the system.");
        }*/

        //allow_update = 1
        //do we just have allow_read - readonly Datatable
        if ($permissionsSum > 1) {//must have edit


            $edit = true;
            //or allow_update 2
            //or allow_create 4
            //or allow_delete 8
            //or allow_execute

            if ($permissionsSum >= 8) { //delete
                $buttons = $buttons . "{extend:'remove', editor: editor},";
                $permissionsSum -= 8;
            }
            //New if via form at bottom of page
            if($permissionsSum>=4) {//create
                $buttons=$buttons."{extend:'create', editor: editor},";
                $permissionsSum-=4;
            }

            if ($permissionsSum >= 2) {//create
                $buttons = $buttons . "{extend:'edit', editor: editor},";
                $permissionsSum -= 2;
            }

            /*$buttons=$buttons. "
					{
                        extend: 'print',
						title: 'A Lovely Report',
								message: 'This is the message'
					},
					{
                        extend: 'pdfHtml5',
						orientation: 'landscape'
					}

				";*/
            return true;
        }

    }



    /**
     *
     * Returns permissions for this application and logged in user
     * (do this on login and stash in session)
     *
     * @author MB
     * @return collection from the permissions table
     */
    public static function getPermissions()
    {

        //get the logged in user
        if (Auth::check()) {
            $id = Auth::user()->id;

            $user = User::find($id);

            Log::info("User {$user->username} with id $id has logged in!");

        }

        $roles = array();
        $arrPermissions = array();
        $arrObjects = array();

        $appID = self::getApplicationConfig();

        //get the users roles
        $colRoles = $user->roles()->where('application_id', $appID)->get();



        if ($colRoles->count()>0) {
            session(['roles' =>  $colRoles->toArray()]);

            self::extractPermissions($colRoles);

            /*return redirect('/home');*/
            return true;

        } else {//not got any perms for this application

            Log::info("User {$user->username} has no permissions for his application!");
            return false;

        }
    }


    /**
     * Pull the permissions from the users roles and combine them
     * @param $colRoles
     */
    public static function extractPermissions($colRoles){
        $arrPerms = array();


        if ($colRoles->count()>0) {

            foreach($colRoles as $role){
                //grab the perms
                $arrRolePerms = $role->permissions->toArray();
                $arrPerms = array_merge($arrPerms,$arrRolePerms);
            }

            self::extractObjects($arrPerms);

            session(['permissions' =>  $arrPerms]);
        }
    }

    /**
     * Just pull the objects into one array,
     * This can be used to build the menu
     *
     * @param $arrPerms
     *
     */

    public static function extractObjects($arrPerms){

        $arrObjects = array();

        if (count($arrPerms)>0) {
            foreach($arrPerms as $permission) {
                $arrObjects[] = $permission['object'];
            }
        }
        session(['objects' =>  $arrObjects]);

    }



    /**
     * Get the application ID from the database, based on the application name which is set up in
     * @param $appID
     * @return bool
     */
    public static function getApplicationConfig(){

        $failure = false;

        //get the application name - from this apps config file
        $appName = Config::get('application.name');
        //  webUserManager
        if ($appName === null){
            $failure = true;
            $reason = "Application has not been configured.";
        }


        //look in web user manager database for this application name
        $app = Application::where('name', $appName)->first();
        if ($app === null){
            $failure = true;
            $reason = "Application has not been registered in Web User Manager.";
        }

        //bomb out if not configured
        if(!$failure) {
            return $app->id;
        }else{
            Auth::logout(); //log the user out
            Session::flush(); //need to clear the session
            App::abort(500, $reason);
        }

    }

    /**
     * See if we have any permissions for this object.
     * This needs moving to middleware.
     * This works a bit like octal permissions:
     * allow_read    = 1
     * allow_update  = 2
     * allow_create  = 4
     * allow_delete  = 8
     * allow_execute = 16
     * They are just added together, to get the permission sum for this object
     *
     * @param $request
     * @param $object
     * @return array
     */
    public static function getPermissionSum($request,$object){

       //use this to test what happens when we lose the session
       // $request->session()->forget('permissions');

        //check permissions here - this should be somewhere resuable
        $permissions = $request->session()->get('permissions');

        //MB 20/09/2016 fixing the log in bug
        if (!$permissions){//lost the session

            $id = Auth::user()->id;
          //  Log::info("User $id has lost session, getting permissions.");
          //  self::getPermissions();
            //$permissions = $request->session()->get('permissions');

          Log::info("User $id has lost session (AccessManager), forcing logout!");
            \App::abort(302, '', ['Location' => 'logout']);

        }

        $permissionsSum = 0;

        //go through the roles and look for this object in the permissions
        foreach($permissions as $permission){ //this is the line which is throwing an error
            //now go through the permissions in this role
            //foreach ($permissions as $permission){
                if (($permission['object']===$object) ){
                    //we have a hit for this permission, add to the return array
                    if($permission['allow_read']===1){
                        $permissionsSum +=1;
                    }
                    if($permission['allow_update']===1){
                        $permissionsSum +=2;
                    }
                    if($permission['allow_create']===1){
                        $permissionsSum +=4;
                    }
                    if($permission['allow_delete']===1){
                        $permissionsSum +=8;
                    }
                    if($permission['allow_execute']===1){
                        $permissionsSum +=16;
                    }
                }
       //     }
        }


        return $permissionsSum;

    }

    public static function getSpecificPermission($request,$name){

        $gotPermission = false;
        //check permissions here - this should be somewhere resuable
        $permissions = $request->session()->get('permissions');
        /*$returnArray = array();*/


            //now go through the permissions in this role
            foreach ($permissions as $permission) {
                if (($permission['name'] === $name)) {
                    $gotPermission = true;
                }

            }

        return $gotPermission;

    }

    /**
     * Just does what it says on the tin.
     *
     * @param $request
     * @return bool
     */
    public static function isAdmin()
    {
        $admin = false;


        $adminRole = Config::get('application.admin_role');

        $roles = session('roles');
        foreach ($roles as $role){

        if ($role['name'] = $adminRole) {
                $admin = true;
        }
        }
        return $admin;

    }

    /**
     * Users who can only see the reports
     * must go to the correct landing page
     *
     *
     * @return bool
     */
    public static function reportsOnly()
    {
        $reportsOnly = false;

       //reporting_role' => 'selection_report_viewer'

        $reportingRole = Config::get('application.reporting_role');

        $roles = session('roles');
        //if we only have one role
        if (count($roles)==1){

            if( $roles[0]['name'] == $reportingRole) {
                $reportsOnly = true;
            }
        }


        return $reportsOnly;

    }




}





