<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //These are the auth rules, normally they are for
        // a table and are obtained from the Web User Manager
        $gate->define('process_types', function($user){
            return $user->canDo('process_types');
        });

        $gate->define('streams', function($user){
            return $user->canDo('streams');
        });

        $gate->define('view_process_loss_users', function($user){
            return $user->canDo('view_process_loss_users');
        });

        $gate->define('fault_type_process_type', function($user){
            return $user->canDo('fault_type_process_type');
        });












    }
}
