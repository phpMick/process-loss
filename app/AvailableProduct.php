<?php

/*
 * Available products are products that are on the current plan.
 * They come from the Selection Database (and are imported from the Excel weekly plans)
 *
 */


namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailableProduct extends Model
{
    protected $connection = 'mysqlSelectionDB';
}
