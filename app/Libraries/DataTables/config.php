<?php if (!defined('DATATABLES')) exit(); // Ensure being used in DataTables env.

// Enable error reporting for debugging (remove for production)
error_reporting(E_ALL);
ini_set('display_errors', '1');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Database user / pass
 */
$sql_details = array(
	"type" => "Mysql",  // Database type: "Mysql", "Postgres", "Sqlite" or "Sqlserver"
	"user" => env('DB_USERNAME', 'forge'),       // Database user name
	"pass" => env('DB_PASSWORD', ''),       // Database password
	"host" => env('DB_HOST', 'localhost'),
	"port" => "",       // Database connection port (can be left empty for default)
	"db"   => env('DB_DATABASE', 'forge'),       // Database name
	"dsn"  => "charset=utf8"        // PHP DSN extra information. Set as `charset=utf8` if you are using MySQL
);



