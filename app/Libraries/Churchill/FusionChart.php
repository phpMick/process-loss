<?php

namespace App\Libraries\Churchill;
//http://www.fusioncharts.com/dev/getting-started/list-of-charts.html

//SINGLE SERIES
//single series column: data {pairs}

//MULTI-SERIES
//multi-series line categories, dataset
//2 series column categories, dataset
//column + line : categories, dataset, "line series"

//with trendlines
//gauge : colorrange, dials
//pie : data

//can I just have the JSON - in the widgets table and add the tokens


abstract class FusionChart

{

    protected $populatedJSON; //Widgets table field

    protected $dataArray = array(); //array which holds the original records from the db
    protected $dataset; // collection which get populated as we process

    //for multiseries charts
    protected $multiSeries = false;

    protected $seriesCollection; //series names
    protected $labelCollection; //labels needed for the category array

    //for dualCharts (like combined bar and line)
    public $dualChart = false;

    protected $formattedData; // the return JSON


    /**
     * FusionChart constructor.
     * The data could be:
     *      1, dualChart (like a bar with a line on)
     *      2, multiSeries (just one graph but multiple series)
     *
     * @param $populatedJSON
     * @param $records
     */
    public function  __construct($populatedJSON, $records){

       $this->dataset = collect([]);

        if(sizeof($records)>1){//dualChart = 2 elements in the records array
            $this->dualChart = true; //eg bar and graph
        }

        //fill the object level variables
        $this->dataArray = $records;
        $this->populatedJSON = $populatedJSON;

        //is this multiseries?
        if (str_contains($this->populatedJSON, '{{ARRAY_DATASET}}') == true) {

            $this->multiSeries = true; //eg multiple series in a dataset

            //I just do this to create the category element
            $seriesCollection = collect($this->dataArray[0]);
            $this->seriesCollection = $seriesCollection->unique('series');
            $this->labelCollection = $seriesCollection->unique('label');
        }

        $this->formatData();
}

    /**
     *
     * Just the getter for the finished JSON
     * @return mixed
     *
     */
    public function getChartDataSource()
    {
        return $this->formattedData;
    }


    /**
     * Make and array for the categories element.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function makeCategoriesArray(){

        //just make the categories array
        $categories= $this->makeChildren( 'label', $this->labelCollection);
        return collect([['category' => $categories]]);

    }


    /**
     * This add a data series to the dataset element
     *  only multiseries data gets here.
     *
     * @param $chartData is an array resultset from the db
     *
     */
    protected function buildSeriesElement($chartData){
        //turn that into a collection
        $chartDataCollection = collect($chartData);

        //get the list of series to loop through
        $seriesCollection = collect($chartData)->unique('series');

        //go through each series, making the data element
        //this series may have a different renderAs - which can be used to add a line to a bar chart

        foreach ($seriesCollection as $series) {
            //just filter the data on this series
            $filteredCollection = $chartDataCollection->where('series', $series->series);
            //now make the data element
            $dataChild = $this->makeChildren('value', $filteredCollection);

            //construct this series and add it
            $this->dataset = $this->dataset->push($this->makeSeriesElement($series,$dataChild));

        }//end foreach
    }


    /**
     * Construct the series element
     * @param $series
     * @param $dataChild
     */
    protected function makeSeriesElement($series,$dataChild){
        //build up the series element
        $datasetArray ['seriesname'] = $series->series;

        //might be adding a line
        if($series->renderas !=""){
            $datasetArray['renderas'] =  $series->renderas;
        }
        $datasetArray['data'] = $dataChild;

        return $datasetArray;

    }


    /**
     * This makes a JSON object with the name $label
     *  which contain the elements in collection
     *
     * @param $label
     * @param $collection
     * @return array
     */

    private function makeChildren( $label, $collection)

    {
        $dataArray = array();

        foreach ($collection as $row) {
            array_push($dataArray, array($label => $row->$label));
        }

        return $dataArray;
    }


}