<?php

namespace App\Libraries\Churchill;
use App\Libraries\Churchill\FusionChart;

//Not sure about multi-series/chart in here

class Gauge extends FusionChart

{

    /**
     * Just makes a decision for single or multiple series
     *
     */
    protected function formatData()
    {
        if ($this->multiSeries) {
             $this->formatMultiSeriesData();

        } else {
             $this->formatSingleSeriesData();
        }
    }


    /**
     * In this case, we will have a label and value in the data (SQL must do this)
     *
     */
    private function formatSingleSeriesData(){

        $value = $this->dataArray[0][0]->value;

        $this->formattedJSON  = str_replace('{{VALUE_VALUE}}',$value,$this->populatedJSON);
    }


    /**
     * This is more complicated when we have multi series data.
     *
     */
    private function formatMultiSeriesData(){

        $categoryLabels = $this->makeCategoriesArray();

        //go through the records array
        foreach($this->dataArray as $chartData){
            $this->buildSeriesElement($chartData);
        }

        //replace the tokens with the data
        $this->formattedJSON =  str_replace('{{ARRAY_CATEGORIES}}',$categoryLabels,$this->populatedJSON);
        $this->formattedJSON =   str_replace('{{ARRAY_DATASET}}',$this->dataset,$this->formattedJSON);

    }

}