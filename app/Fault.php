<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fault extends Model {


    protected $connection = 'mysqlSelectionDB';


    protected $table = 'faults';
	public $timestamps = true;

	use SoftDeletes;

	protected $dates = ['deleted_at'];
	protected $visible = array('description');


    /**
     * Returns  Process Type which can have this fault
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function processTypes()
    {

        return $this->belongsToMany('App\ProcessTypes');

    }


}