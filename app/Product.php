<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Elasticquent\ElasticquentTrait;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;


class Product extends Model
{
    use SoftDeletes;
    use ElasticquentTrait;


    protected $table = 'products';
    public $timestamps = true;

    protected $connection = 'mysqlProductsDB';


    /**
     *
     * Tests if product is in available products
     *
     * @param $part_no
     */
    public static function availableProduct($product_id){

        $weeksAvailableFor = Config::get('application.products_weeks_available');

        $availableProducts = AvailableProduct::where('product_id', '=', $product_id)
            ->where('made_available', '>', Carbon::now()->addWeeks(-$weeksAvailableFor))
            ->get();

        if ($availableProducts->count() > 0) {// available product
            return true;
        }else{
            return false;
        }
    }


    /**
     * Ajax POST when the user leaves the Typeahead
     * Validates the product code
     * $retArray['status'] can be :
     *  available - product is in the available products table
     *  valid - not in available but still a valid part
     *  if not valid 404 is thrown
     *
     * @param $searchString
     * @return \Illuminate\Http\JsonResponse
     */
    public static function validatePartNumber($part_no)
    {
        $allowUnavailable = Config::get('application.allow_products_not_in_available_products');
        $retArray = array();

        $product = Product::where('part_no','=',$part_no)->first();

        if(count($product)){ //do we have a product - therefore this is a valid part number

            $retArray['product_id'] = $product->id;
            $retArray['product_description'] = $product->product_description;

            if(Product::availableProduct($product->id)){//product is valid and in available products
                $retArray['status'] = 'available';
                return response()->json( $retArray);

            }else{//product is valid but not available
                $retArray['status'] = 'valid';

                if($allowUnavailable) {
                    return response()->json( $retArray);
                }else {
                    return response()->json(['error' => 'Product is not in available products.'], 404); // Status code here
                }
            }
        }else{//not valid product
            return response()->json(['error' => 'Not a valid part number.'], 404); // Status code here
        }
    }






}
