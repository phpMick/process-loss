<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Role extends Model
{

    use SoftDeletes;

    protected $connection = 'mysqlUserDB';

    protected $fillable = ['name','application_id','permission_id'];

    public function application()
    {
        return $this->belongsTo('App\Application');
    }


    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

    /**
     * many-to-many relationship method.
     *
     * @return QueryBuilder
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }








}
