<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;

    protected $connection = 'mysqlUserDB';

    protected $fillable = ['name'];



    public function roles()
    {
        return $this->hasMany('App\Role');
    }

}
