<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mysqlUserDB';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    /**
     * Returns roles for this user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }


    /**
     * Tests that user has this role
     * @param $roleName
     * @return bool
     */

    public function hasRole($roleName)
    {
        foreach ($this->roles()->get() as $role) {
            if ($role->name == $roleName) {
                return true;
            }
        }

        return false;
    }


    /**
     * Checks that user can perform this action
     *
     * @param $key
     * @return mixed
     */
    public function canDo($key)
    {
        $keys = Session::get('objects');

        //todo test this
        if($keys != null){

            return in_array($key, $keys);

        }
    }


    ///These are for this application

    /**
     * Returns process types for this user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function processTypes()
    {
        return $this->belongsToMany('App\ProcessType');
    }


    /**
     * Get all of the faults for this user
     * through process types.
     * Couldn't get this with Have Many Through
     *
     */
    public function allowedProcessTypeFaults()
    {

        /*$SQL = "SELECT f.id,f.description FROM process_type_user p2u
                    RIGHT JOIN process_types pt on pt.id = p2u.process_type_id
                    RIGHT JOIN fault_type_process_type ftpt on ftpt.fault_type_process_type = p2u.process_type_id
                    INNER JOIN dev_selection.faults f on f.id = ftpt.fault_id
                    WHERE p2u.user_id = 1";

        $faults = DB::raw($SQL)->get();*/


        //the Selection Database will have a different name in live;
        $selectionDB = env('DB_DATABASE_SELECTIONDB');



        $faults = DB::table('process_type_user')
            ->rightJoin('process_types', 'id', '=', 'process_type_user.process_type_id')
            ->rightJoin('fault_type_process_type', 'fault_type_process_type.process_type_id', '=',
                'process_type_user.process_type_id')
            ->join($selectionDB . '.faults', $selectionDB . '.faults.id', '=', 'fault_type_process_type.fault_id')
            ->select('faults.description', 'faults.id', 'process_types.id as process_type_id')
            ->get();

        return $faults;
    }


    /**
     * Only get process types which have faults assigned
     * these are the only buttons we want to display
     *
     * @return mixed#
     *
     */
    public function processTypesWithFaults()
    {
        //this is what we want to do:
        //SELECT DISTINCT pt.id,pt.description from process_types pt
        //INNER JOIN fault_type_process_type ftpt ON pt.id = ftpt.process_type_id

        $processTypes = DB::table('process_type_user')

            ->join('process_types','process_types.id','=','process_type_user.process_type_id')
            ->join('fault_type_process_type','fault_type_process_type.process_type_id','=','process_types.id')
            ->distinct()
            ->select('process_types.id','process_types.description')
            ->where('process_type_user.user_id','=',$this->id)
            ->get();

        return $processTypes;
    }


}
