const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');






/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(function(mix) {

    mix.sass('app.scss')
        .webpack('app.js');



    mix.styles([
        'dashboard.css',
        'editor.dataTables.min.css',
        'font-awesome.min.css',
        'jquery-ui.css',
        'bootstrap.min.css',
        'buttons.dataTables.min.css',
        'select.dataTables.min.css',
        'dataTables.bootstrap.min.css',
        'select2.min.css',
        'typeahead.css',
        'churchill.css',
        'toastr.css'
    ],'public/css/all.css');

    mix.styles([
        'typeahead.css',
        'churchill-tablet.css'
    ],'public/css/tablet.css');

    mix.scripts([
        'jquery-3.1.1.js',
        'moment.min.js',
        'jquery-ui.js',
        'lodash.js',
        'gridstack.min.js',
        'vue.js',
        'jquery.dataTables.js',
        'dataTables.buttons.min.js',
        'dataTables.select.min.js',
        'dataTables.editor.js',
        'editor.select2.js',
        'jszip.min.js',
        'vfs_fonts.js',
        'buttons.html5.min.js',
        'buttons.print.min.js',
        'dataTables.responsive.min.js',
        'dataTables.bootstrap.min.js',
        'axios.js',
        'select2.min.js'

    ], 'public/js/all.js')

        .scripts([
            'typeahead.bundle.js',
            'toastr.min.js'

        ],'public/js/tablet.js');


    mix.version(['css/all.css','css/tablet.css', 'js/all.js','js/app.js','js/tablet.js']);

});









