<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaultTypeProcessTypeTable extends Migration {

	public function up()
	{
		Schema::create('fault_type_process_type', function(Blueprint $table) {
            $table->increments('id');
			$table->integer('fault_id')->unsigned();
			$table->integer('process_type_id')->unsigned();
			$table->integer('stream_id')->unsigned();
            $table->integer('rank')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('fault_type_process_type');
	}
}