<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInputTypesTable extends Migration {

	public function up()
	{
		Schema::create('input_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('description', 255);
		});
	}

	public function down()
	{
		Schema::drop('input_types');
	}
}