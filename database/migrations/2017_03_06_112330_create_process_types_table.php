<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessTypesTable extends Migration {

	public function up()
	{
		Schema::create('process_types', function(Blueprint $table) {
			$table->increments('id');
			$table->string('description', 255);
			$table->integer('input_type_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('process_types');
	}
}