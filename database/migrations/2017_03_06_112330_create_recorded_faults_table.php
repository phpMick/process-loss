<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecordedFaultsTable extends Migration {

	public function up()
	{
		Schema::create('recorded_faults', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('product_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('fault_type_id')->unsigned();
            $table->boolean('primary_fault');
		});
	}

	public function down()
	{
		Schema::drop('recorded_faults');
	}
}