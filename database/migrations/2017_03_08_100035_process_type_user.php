<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProcessTypeUser extends Migration
{
    public function up()
    {
        Schema::create('process_type_user', function(Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('process_type_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::drop('process_type_user');
    }
}
