<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(Input_TypesTableSeeder::class);
        $this->call(Process_TypesTableSeeder::class);
        $this->call(Recorded_FaultsTableSeeder::class);
        $this->call(StreamsTableSeeder::class);
        $this->call(FaultTypeProcessTypeTableSeeder::class);
        $this->call(ProcessTypeUserTableSeeder::class);

    }
}
