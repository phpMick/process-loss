<?php

use Illuminate\Database\Seeder;

class FaultTypeProcessTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fault_type_process_type')->insert([
            [
                'id' => "1",
                'fault_id' => "1",
                'process_type_id'=> "1",
                'stream_id'=> "2",
                'rank'=> "1"
            ],
            [
                'id' => "2",
                'fault_id' => "1097",
                'process_type_id'=> "1",
                'stream_id'=> "2",
                'rank'=> "2"
            ],
            [
                'id' => "3",
                'fault_id' => "1158",
                'process_type_id'=> "1",
                'stream_id'=> "2",
                'rank'=> "3"
            ],
            [
                'id' => "4",
                'fault_id' => "17",
                'process_type_id'=> "1",
                'stream_id'=> "2",
                'rank'=> "4"
            ],
            [
                'id' => "5",
                'fault_id' => "1066",
                'process_type_id'=> "2",
                'stream_id'=> "2",
                'rank'=> "5"
            ],
            [
                'id' => "6",
                'fault_id' => "42",
                'process_type_id'=> "1",
                'stream_id'=> "2",
                'rank'=> "6"
            ],
            [
                'id' => "7",
                'fault_id' => "56",
                'process_type_id'=> "3",
                'stream_id'=> "2",
                'rank'=> "7"
            ],
            [
                'id' => "8",
                'fault_id' => "16",
                'process_type_id'=> "1",
                'stream_id'=> "1",
                'rank'=> "8"
            ],
            [
                'id' => "9",
                'fault_id' => "22",
                'process_type_id'=> "1",
                'stream_id'=> "1",
                'rank'=> "9"
            ]


        ]);


    }
}
