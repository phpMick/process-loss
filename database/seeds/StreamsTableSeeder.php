<?php

use Illuminate\Database\Seeder;

class StreamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('streams')->insert([
            [
                'id' => "1",
                'description'=> "CUPS"
            ],
            [
                'id' => "2",
                'description'=> "FLAT"
            ],
            [
                'id' => "3",
                'description'=> "RDISH"
            ],
            [
                'id' => "4",
                'description'=> "HOLL"
            ],
            [
                'id' => "5",
                'description'=> "PCAST"
            ],
            [
                'id' => "6",
                'description'=> "SOURC"
            ],
            [
                'id' => "7",
                'description'=> "DISH"
            ],
            [
                'id' => "8",
                'description'=> "LIT"
            ]

        ]);

    }
}
