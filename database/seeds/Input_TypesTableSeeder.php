<?php

use Illuminate\Database\Seeder;

class Input_TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('input_types')->insert([
            [
                'id' => "1",
                'description'=> "product_code"
            ],
            [
                'id' => "2",
                'description'=> "pattern_shape"
            ]


        ]);

    }
}
