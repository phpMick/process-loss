<?php

use Illuminate\Database\Seeder;

class Recorded_FaultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('recorded_faults')->insert([
            [
                'id' => "1",
                'product_id'=> "1",
                'user_id'=> "1",
                'fault_type_id'=> "1",
                'primary_fault'=> "1"
            ]


        ]);
    }
}
