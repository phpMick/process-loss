<?php

use Illuminate\Database\Seeder;

class Process_TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('process_types')->insert([
            [
                'id' => "1",
                'description'=> "Glost",
                'input_type_id'=> "1"

            ],
            [
                'id' => "2",
                'description'=> "Biscuit",
                'input_type_id'=> "1"
            ],
            [
                'id' => "3",
                'description'=> "TT2",
                'input_type_id'=> "1"
            ],
            [
                'id' => "4",
                'description'=> "TT4",
                'input_type_id'=> "1"
            ],
            [
                'id' => "5",
                'description'=> "Dekram",
                'input_type_id'=> "1"
            ],
            [
                'id' => "6",
                'description'=> "Malkim",
                'input_type_id'=> "1"
            ],
            [
                'id' => "7",
                'description'=> "Process 4",
                'input_type_id'=> "1"
            ]

        ]);
    }
}



