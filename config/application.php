<?php



    /*
    |--------------------------------------------------------------------------
    | Default Application Constants (MB not Taylor)
    |--------------------------------------------------------------------------
    | Usage: Config::get('application.name');
    |
    */

return [

    'name' => 'process-loss',
    'admin_role' => 'process_loss_admin',
    'products_weeks_available' => 3,
    'allow_products_not_in_available_products' => false,

];




